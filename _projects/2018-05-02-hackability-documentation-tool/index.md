---
layout: project
title: Hackability Documentation Tool
description: "This projects enable users to document their Hackability project and share it with the web"
image: /projects/2018-05-02-hackability-documentation-tool/images/main.png
number: 1.1
level: 1
time: 85
cost: 0
data: 2018-05-02
tools: ['PC','Programmable Elettronics','Embroidery Machine','Large CNC']
why: "Documenting is hard, and we need a tool to enabling users to document their hackability projects. "
what: "The project is a simple web application that enables users participating to an hackability event to document in a standard way their project!"
features: "This is a simple web application developed with Angular(5) and Typescript. The app generates a Jekyll compatible project that can be upload on GitLab"
license: "CC-BY-SA"
youtube: YEh5Y0nIPJU
---

**Documenting is hard**, and probably it's the hardest part of an open source project.

With docs.hackability, we want to provide a simple and zero-cost product helping people documenting their project!

# What I need to build it?

 - A simple PC with internet connecting
 - Some time
 - 

<div class="step">
<div class="info">
<img class="img-resp" src="./images/img_0.png">
</div>
<div class="description">
<div class="main"  markdown="1">
<h2 class="title"> <span> Step 1</span> Think! </h2>
<div class="time"> Time: 30 mins </div>

In order to suceffully document your project, you need to think and answer to some 
questions. This will help you to write good documentation.

The whestions are:

 1. Why you build this project? Which are the problem this project solved?
 2. What the projects does? How it works?
 3. What are the main features of the project?

</div>



</div>

</div>


<div class="step">
<div class="info">
<img class="img-resp" src="./images/img_1.png">
</div>
<div class="description">
<div class="main"  markdown="1">
<h2 class="title"> <span> Step 2</span> Choose Your License </h2>
<div class="time"> Time: 5 mins </div>

You need to decide how users can use and improve your project, and set up a license! 

Choosing a good license is difficult, and often you have no time to dedicate to study all the difference licenses available. So we simplify you life!!

All Hackability project will be release under Creative Common (CC) license, Attribution (BY) Share Alike (SA). This means that all contributors to your project have to:

 1. Mention you as original author of the project
 2. Share the evolution of the project with the same license.

You can choose to not enable contributors to make commercial activity with your project or with any avolution to the project, by adding the Non Commercial (NC) option.

This is up to you, but we suggest not to add NC to your project if you want to encourage diffusion of the project!

</div>


<div class="help" markdown="1">

<h1 class="helpTitle">Help wanted</h1>

I need someone who can help me improve this step!

How can you help me?

 - Write better stuff
 - Grammar check

</div>
      


</div>

</div>


<div class="step">
<div class="info">
<img class="img-resp" src="./images/img_2.png">
</div>
<div class="description">
<div class="main"  markdown="1">
<h2 class="title"> <span> Step 3</span> Divide et Impera </h2>
<div class="time"> Time: 50 mins </div>

This is the hardest part: you need to understand how to divide the construction process of your project in *atomic* steps and describe carefully each step. 

In this way, you'll be able to enable other users to replicate and improve your work!

</div>



</div>

</div>


<div class="contributors" markdown="1">

# Contributors

 - Ludovico Russo [ludusrusso](https://github.com/ludusrusso) developed the main user interface of this tool and the jekyll site able to render it!



</div>
    