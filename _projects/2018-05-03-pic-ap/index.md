---
layout: project
title: PIC-AP
description: "Personal kit and minimum laptop to prepare food."
image: /projects/2018-05-03-pic-ap/images/main.png
level: 1
time: 75
cost: 1
data: 2018-05-03
tools: ['Laser Cutter','Hammer','PC']
why: "What problems does it solve:
order and availability of objects
possibility of arranging according to the desired lighting
possibility to bring the kit where we want (in another room or in another house!)"
what: "Visual requirements: avoid dazzling / transparent materials that do not reflect light
Prepare opaque materials
have chromatic contrasts of at least 70% (consult color contrast tables)
avoid using too many colors
take into account the different situations of low vision"
features: "Toolbox contents:
The kit is custom designed from IKEA kitchen utensils, easy to find and affordable.

chopping board
3 knives
scissor
potato peeler
cheese grater"
license: "CC-BY-SA"
---

Marco, visually impaired, needs an extremely organized workspace to cook in safety and with comfort. Work tools must always be in the same position and be easily distinguishable by contrast and color. PIC-AP offers a "comfort zone": a minimal and personal working environment, where the visually impaired people can work safely, having the certainty of finding his instruments. The functionality of the project makes an appropriate tool to give (or re-give) the ability to prepare food anywhere and independently, making it a product suitable for different types of users. It’s not connoted as equipment for people with disabilities.

# What I need to build it?

- plexiglass 4mm
- wood 10mm, we suggest Beech wood
- natural impregnating agent


<div class="step">
<div class="info">
<img class="img-resp" src="./images/img_0.png">
</div>
<div class="description">
<div class="main"  markdown="1">
<h2 class="title"> <span> Step 1</span> Preparazione dei file </h2>
<div class="time"> Time: 20 mins </div>

Use the files “lasercut” to cut the plexiglass 4 mm 
scarica i file a questo [link](http://)

</div>



</div>

</div>


<div class="step">
<div class="info">
<img class="img-resp" src="./images/img_1.png">
</div>
<div class="description">
<div class="main"  markdown="1">
<h2 class="title"> <span> Step 2</span> assemblaggio </h2>
<div class="time"> Time: 55 mins </div>

Use the files “cnc mill” to cut wood 10mm, we suggest Beech wood

</div>



</div>

</div>


<div class="contributors" markdown="1">

# Contributors


- Marco Boneschi
- Fabrizio De Paoli (designer), info@digitalfucina.com
- Sebastiano Ercoli (designer), sebastiano.ercoli@gmail.com
- Rosa Garofalo, rgarofalo@subvedenti.it
- Alberto Ghirardello (designer, project leader), info@albertoghirardello.com
- Davide Mercenati (designer), davide.mercenati@libero.it
- Francesco Rodighiero (designer, tutor), francesco.rodighiero@gmail.com
- Nicolò Venturi (designer), nicovallauri@gmail.com
- Ludovico Russo (documentation, tutor), ludus.russo@gmail.com

</div>
    