---
layout: tutorial
num: 4.41
title: Pin game
description: "Pin board with pins of different colors and shapes. to learn to count, to recognize colors and shapes."
image: /tutorials/2018-06-17-pin-game/images/main.png
level: 3
time: 0
cost: 1
data: 2018-06-17
tools: ['ruler','brush','caliber']
machines: ['lasercutter']
skills: ['2d']
license: "CC-BY-SA"
topic: computer-controlled-cutting-laser
---

Pin board with pins of different colors and shapes. to learn to count, to recognize colors and shapes.

# What I need to build it?



<div class="contributors" markdown="1">

# Contributors

Laura Cipriani

</div>
    