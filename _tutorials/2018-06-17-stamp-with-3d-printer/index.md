---
layout: tutorial
num: 6.40
title: stamp with 3d printer
description: "3D printing spinning top"
image: /tutorials/2018-06-17-stamp-with-3d-printer/images/main.png
level: 3
time: 0
cost: 1
data: 2018-06-17
tools: ['ruler','cutter']
machines: ['3dprinter']
skills: ['2d','3d']
license: "CC-BY-SA"
topic: 3d-printing
---

3D printing spinning top

# What I need to build it?



<div class="contributors" markdown="1">

# Contributors

Laura Cipriani

</div>
    