webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\n<!--\n\n<md-editor name=\"Content\" [preRender]=\"preRenderFunc\" [(ngModel)]=\"content\" [height]=\"'200px'\" [mode]=\"mode\" [options]=\"options\" required maxlength=\"500\"></md-editor>\n-->\n\n<div class=\"grid\">\n  <div>\n      <label> Id \n        <input type=\"text\" name=\"id\" [(ngModel)]=\"docs.meta.id\">\n      </label>  \n    <label> Title \n      <input type=\"text\" name=\"title\" [(ngModel)]=\"docs.meta.title\">\n    </label>\n    <label> Topic\n      <select [(ngModel)]=\"docs.meta.topic\">\n        <option *ngFor=\"let topic of docs.topics\" [ngValue]=\"topic\"> {{topic}}</option>\n      </select>\n    </label>\n  </div>\n  <div class=\"image\"> <label for=\"\"> Image </label>\n      <app-image-manager [(image)]=\"docs.meta.image\"></app-image-manager>\n      <label> <strong> Long Description of the Project</strong></label>\n      <md-editor \n        name=\"Content\" \n        [(ngModel)]=\"docs.meta.description\" \n        [height]=\"'400px'\" \n        required maxlength=\"500\">\n      </md-editor>\n\n\n      <div *ngFor=\"let items_type of ['skills', 'tools', 'machines']\">\n          <h4>{{items_type}} Needed</h4>\n          <div class=\"toolsUsed\">\n            <button *ngFor=\"let item of docs.availableItems(items_type)\" (click)=\"docs.addItem(items_type, item)\"> {{item}} </button>\n          </div>\n    \n          <h5>Selected {{items_type}}</h5>\n          <div class=\"toolsAvailable\">\n            <button *ngFor=\"let item of docs.usedItems(items_type)\" (click)=\"docs.removeItem(items_type, item)\"> {{item}} </button>\n          </div>\n    \n      </div>\n\n      \n      <label> <strong>What you need to build it?</strong></label>\n      <md-editor \n        name=\"Content\" \n        [(ngModel)]=\"docs.meta.materials\" \n        [height]=\"'400px'\" \n        required maxlength=\"1000\">\n      </md-editor>\n\n  </div>\n\n  <div class=\"data\">\n    <form action=\"\">\n      <label>\n        <strong>What's the project in a sentence</strong>\n        <textarea name=\"brief\" id=\"\" rows=\"4\" [(ngModel)]=\"docs.meta.brief\"></textarea>\n      </label>\n      <label>\n        <strong>Difficulty Level</strong> {{ docs.meta.level }}\n        <input name=\"level\" type=\"range\" min=\"0\" max=\"4\" step=\"1\" [(ngModel)]=\"docs.meta.level\">\n      </label>\n      <label>\n        <strong>Contruction Time (automatically Computed)</strong>\n           {{ docs.buildTime() | extTime}}\n        </label>\n      <label> <strong>YouTube</strong>\n        <input type=\"text\" name=\"youtube\" placeholder=\"Insert Youtube ID here\" [(ngModel)]=\"docs.meta.youtube\">\n      </label>\n      <div *ngIf=\"docs.meta.youtube\"> \n        <a class=\"button\" role=\"button\" href=\"https://www.youtube.com/watch?v={{docs.meta.youtube}}\" target=\"_blank\"> Check video</a>\n      </div>\n\n  \n      <h4>Credits</h4>\n      <label> <strong> List all persons and organizations involed in the development of the projects</strong></label>\n      <md-editor \n        name=\"Content\" \n        [(ngModel)]=\"docs.meta.credits\" \n        [height]=\"'400px'\"\n        required maxlength=\"500\">\n      </md-editor>\n\n      <h4>License</h4>\n      <label>\n        <input type=\"checkbox\" name=\"help\" [(ngModel)]=\"docs.meta.isCommercial\"/> I Want to enable users to commercialize derivates of the project\n      </label>\n      <span>{{docs.license()}}</span>\n    </form>\n  </div>\n</div>\n\n<div class=\"container\">\n  <app-step-description></app-step-description>\n  <button class=\"button\" (click)=\"save()\">Save</button>\n  <button class=\"button\" (click)=\"download()\">Download</button>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/***/ (function(module, exports) {

module.exports = ".grid {\n  display: -ms-grid;\n  display: grid;\n  -ms-grid-columns: 1fr;\n      grid-template-columns: 1fr;\n  grid-gap: 10px; }\n  .grid div {\n    padding: 20px; }\n  .grid .toolsUsed, .grid .toolsAvailable {\n    padding: 0;\n    margin-bottom: 20px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex; }\n  .grid .toolsUsed button, .grid .toolsAvailable button {\n      padding: 5px;\n      background-color: #eee;\n      margin-right: 10px; }\n  .container {\n  margin: 0;\n  padding: 20px;\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__docs_service__ = __webpack_require__("./src/app/docs.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__github_manager_service__ = __webpack_require__("./src/app/github-manager.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(docs, github) {
        this.docs = docs;
        this.github = github;
        this.level = [
            'very easy',
            'easy',
            'medium',
            'hard',
            'ninja'
        ];
        this.cost = [
            '< 50 €',
            '50€ - 250€',
            '250€ - 500€',
            '500k€ - 1k€',
            '> 1k€'
        ];
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent.prototype.save = function () {
        this.docs.save();
    };
    AppComponent.prototype.download = function () {
        this.docs.downlaod();
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__docs_service__["a" /* DocsService */],
            __WEBPACK_IMPORTED_MODULE_2__github_manager_service__["a" /* GithubManagerService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_markdown_editor__ = __webpack_require__("./node_modules/ngx-markdown-editor/esm5/ngx-markdown-editor.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ng_dynamic_forms_core__ = __webpack_require__("./node_modules/@ng-dynamic-forms/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ng_dynamic_forms_ui_foundation__ = __webpack_require__("./node_modules/@ng-dynamic-forms/ui-foundation/esm5/ui-foundation.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_image_cropper__ = __webpack_require__("./node_modules/ngx-image-cropper/ngx-image-cropper.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__image_manager_image_manager_component__ = __webpack_require__("./src/app/image-manager/image-manager.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__step_description_step_description_component__ = __webpack_require__("./src/app/step-description/step-description.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__docs_service__ = __webpack_require__("./src/app/docs.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ext_time_pipe__ = __webpack_require__("./src/app/ext-time.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__github_manager_service__ = __webpack_require__("./src/app/github-manager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_8__image_manager_image_manager_component__["a" /* ImageManagerComponent */],
                __WEBPACK_IMPORTED_MODULE_9__step_description_step_description_component__["a" /* StepDescriptionComponent */],
                __WEBPACK_IMPORTED_MODULE_11__ext_time_pipe__["a" /* ExtTimePipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_3_ngx_markdown_editor__["a" /* LMarkdownEditorModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_5__ng_dynamic_forms_core__["m" /* DynamicFormsCoreModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6__ng_dynamic_forms_ui_foundation__["a" /* DynamicFormsFoundationUIModule */],
                __WEBPACK_IMPORTED_MODULE_7_ngx_image_cropper__["a" /* ImageCropperModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_common_http__["b" /* HttpClientModule */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_10__docs_service__["a" /* DocsService */],
                __WEBPACK_IMPORTED_MODULE_12__github_manager_service__["a" /* GithubManagerService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/docs.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_jszip__ = __webpack_require__("./node_modules/jszip/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_jszip___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_jszip__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils__ = __webpack_require__("./src/app/utils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_file_saver_FileSaver__ = __webpack_require__("./node_modules/file-saver/FileSaver.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_file_saver_FileSaver___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_file_saver_FileSaver__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var INFO = {
    "machines": new Set([
        "vinylcutter",
        "milling-machine",
        "lasercutter",
        "3dprinter",
    ]),
    "tools": new Set([
        "wrench",
        "screwdriver",
        "screw",
        "scissors",
        "saw",
        "ruler",
        "pliers",
        "pincers",
        "nail",
        "micro-controller",
        "hot-glue",
        "helmet",
        "hammer",
        "drill",
        "cutter",
        "compass",
        "clamp",
        "caliber",
        "brush",
    ]),
    "topics": [
        "project-management",
        "electronics-production",
        "computer-controlling-machining",
        "computer-controlled-cutting-vinyl",
        "computer-controlled-cutting-laser",
        "3d-printing",
        "3d-drawing",
        "2d-drawing",
    ],
    "skills": new Set([
        "git",
        "electronics",
        "3d",
        "2d",
    ])
};
var DocsService = /** @class */ (function () {
    function DocsService(http) {
        this.http = http;
        this.meta = {};
        this.tools = INFO.tools;
        this.machines = INFO.machines;
        this.skills = INFO.skills;
        this.topics = INFO.topics;
        var data = this.load();
        if (data === null) {
            this.meta = {
                level: 3,
                time: 90,
                cost: 1,
                steps: [],
                tools: new Set([]),
                skills: new Set([]),
                machines: new Set([]),
            };
        }
        else {
            this.meta = data;
        }
        console.log(this.machines);
    }
    DocsService.prototype.save = function () {
        var toStore = Object.assign({}, this.meta);
        toStore.tools = Array.from(this.meta.tools);
        toStore.skills = Array.from(this.meta.skills);
        toStore.machines = Array.from(this.meta.machines);
        localStorage.setItem('meta', JSON.stringify(toStore));
    };
    DocsService.prototype.load = function () {
        var stored = JSON.parse(localStorage.getItem('meta'));
        if (stored) {
            stored.tools = new Set(stored.tools);
            stored.skills = new Set(stored.skills);
            stored.machines = new Set(stored.machines);
        }
        return stored;
    };
    DocsService.prototype.addStep = function () {
        this.meta.steps.push({
            title: '',
            description: '',
            image: undefined,
            time: 5,
            helpWanted: false,
        });
    };
    DocsService.prototype.license = function () {
        var lic = 'CC-';
        if (!this.meta.isCommercial) {
            lic += 'NC-';
        }
        lic += 'BY-SA';
        return lic;
    };
    DocsService.prototype.availableItems = function (items_type) {
        var _this = this;
        var ret = new Set(Array.from(this[items_type]).filter(function (x) { return !_this.meta[items_type].has(x); }));
        return ret;
    };
    DocsService.prototype.usedItems = function (items_type) {
        return this.meta[items_type];
    };
    DocsService.prototype.addItem = function (items_type, item) {
        this.meta[items_type].add(item);
    };
    DocsService.prototype.removeItem = function (items_type, item) {
        this.meta[items_type].delete(item);
    };
    DocsService.prototype.step2text = function (step, index) {
        var help = '';
        if (step.helpWanted) {
            help = "\n<div class=\"help\" markdown=\"1\">\n\n<h1 class=\"helpTitle\">Help wanted</h1>\n\n" + step.helpDescription + "\n\n</div>\n      \n";
        }
        var data = "\n<div class=\"step\">\n<div class=\"info\">\n<img class=\"img-resp\" src=\"./images/img_" + index + ".png\">\n</div>\n<div class=\"description\">\n<div class=\"main\"  markdown=\"1\">\n<h2 class=\"title\"> <span> Step " + (index + 1) + "</span> " + step.title + " </h2>\n<div class=\"time\"> Time: " + step.time + " mins </div>\n\n" + step.description + "\n\n</div>\n\n" + help + "\n\n</div>\n\n</div>\n\n";
        return data;
    };
    DocsService.prototype.meta2text = function () {
        if (!this.meta.date) {
            this.meta.date = new Date().toISOString().substring(0, 10);
        }
        var data = "---\nlayout: tutorial\nnum: " + this.meta.id + "\ntitle: " + this.meta.title + "\ndescription: \"" + this.meta.brief + "\"\nimage: /tutorials/" + this.folderGen() + "/images/main.png\nlevel: " + this.meta.level + "\ntime: " + this.buildTime() + "\ncost: " + this.meta.cost + "\ndata: " + this.meta.date + "\ntools: [" + Array.from(this.usedItems('tools')).map(function (tool) { return "'" + tool + "'"; }) + "]\nmachines: [" + Array.from(this.usedItems('machines')).map(function (tool) { return "'" + tool + "'"; }) + "]\nskills: [" + Array.from(this.usedItems('skills')).map(function (tool) { return "'" + tool + "'"; }) + "]\nlicense: \"" + this.license() + "\"\ntopic: " + this.meta.topic;
        if (this.meta.youtube) {
            data += "\nyoutube: " + this.meta.youtube;
        }
        data += "\n---\n\n" + this.meta.description + "\n\n# What I need to build it?\n\n" + this.meta.materials + "\n";
        return data;
    };
    DocsService.prototype.closingToText = function () {
        return "\n<div class=\"contributors\" markdown=\"1\">\n\n# Contributors\n\n" + this.meta.credits + "\n\n</div>\n    ";
    };
    DocsService.prototype.toText = function () {
        var data = this.meta2text();
        for (var i = 0; i < this.meta.steps.length; i++) {
            var step = this.meta.steps[i];
            data += this.step2text(step, i);
        }
        data += this.closingToText();
        return data;
    };
    DocsService.prototype.folderGen = function () {
        return this.meta.date + "-" + Object(__WEBPACK_IMPORTED_MODULE_2__utils__["a" /* slugify */])(this.meta.title);
    };
    DocsService.prototype.downlaod = function () {
        var _this = this;
        this.save();
        var zip = new __WEBPACK_IMPORTED_MODULE_1_jszip__();
        zip.file("index.md", this.toText());
        var img = zip.folder("images");
        img.file("main.png", this.meta.image.split(',')[1], { base64: true });
        this.meta.steps.forEach(function (step, idx) {
            img.file("img_" + idx + ".png", step.image.split(',')[1], { base64: true });
        });
        zip.file("data.json", JSON.stringify(this.meta));
        zip.generateAsync({ type: "blob" })
            .then(function (content) {
            Object(__WEBPACK_IMPORTED_MODULE_3_file_saver_FileSaver__["saveAs"])(content, _this.folderGen() + ".zip");
        });
    };
    DocsService.prototype.buildTime = function () {
        return this.meta.steps.map(function (step) { return step.time; }).reduce(function (acc, t) { return acc + t; }, 0);
    };
    DocsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */]])
    ], DocsService);
    return DocsService;
}());



/***/ }),

/***/ "./src/app/ext-time.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExtTimePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ExtTimePipe = /** @class */ (function () {
    function ExtTimePipe() {
    }
    ExtTimePipe.prototype.transform = function (mins) {
        var hours = Math.floor(mins / 60);
        var minutes = mins % 60;
        var data = '';
        data += hours + ":" + minutes;
        return data;
    };
    ExtTimePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'extTime'
        })
    ], ExtTimePipe);
    return ExtTimePipe;
}());



/***/ }),

/***/ "./src/app/github-manager.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GithubManagerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_github_api__ = __webpack_require__("./node_modules/github-api/dist/components/GitHub.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_github_api___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_github_api__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GithubManagerService = /** @class */ (function () {
    function GithubManagerService() {
        var gh = new __WEBPACK_IMPORTED_MODULE_1_github_api__({
            username: 'ludusrusso',
            password: 'Lundr1c0'
        });
        var me = gh.getUser();
        console.log(me);
        me.listStarredRepos(function (err, repos) {
            console.log(repos);
        });
    }
    GithubManagerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], GithubManagerService);
    return GithubManagerService;
}());



/***/ }),

/***/ "./src/app/image-manager/image-manager.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"file\" (change)=\"fileChangeEvent($event)\" />\n<div *ngIf=\"toSave\">\n  <image-cropper\n    [imageChangedEvent]=\"imageChangedEvent\"\n    [maintainAspectRatio]=\"true\"\n    [aspectRatio]=\"3 / 2\"\n    [resizeToWidth]=\"600\"\n    format=\"png\"\n    (imageCropped)=\"imageCropped($event)\"\n    (imageLoaded)=\"imageLoaded()\"\n    (loadImageFailed)=\"loadImageFailed()\"\n></image-cropper>\n<button class=\"button\" (click)=\"saveImg()\">Save</button>\n</div>\n<img *ngIf=\"!toSave\" [src]=\"image || 'http://via.placeholder.com/600x400'\" />\n"

/***/ }),

/***/ "./src/app/image-manager/image-manager.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/image-manager/image-manager.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageManagerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ImageManagerComponent = /** @class */ (function () {
    function ImageManagerComponent() {
        this.imageChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.toSave = false;
        this.imageChangedEvent = '';
    }
    ImageManagerComponent.prototype.ngOnInit = function () {
    };
    ImageManagerComponent.prototype.fileChangeEvent = function (event) {
        this.toSave = true;
        this.imageChangedEvent = event;
    };
    ImageManagerComponent.prototype.imageCropped = function (image) {
        this.image = image;
        console.log(image);
    };
    ImageManagerComponent.prototype.imageLoaded = function () {
        // show cropper
    };
    ImageManagerComponent.prototype.loadImageFailed = function () {
        // show message
    };
    ImageManagerComponent.prototype.saveImg = function () {
        this.toSave = false;
        this.imageChange.emit(this.image);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], ImageManagerComponent.prototype, "image", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], ImageManagerComponent.prototype, "imageChange", void 0);
    ImageManagerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-image-manager',
            template: __webpack_require__("./src/app/image-manager/image-manager.component.html"),
            styles: [__webpack_require__("./src/app/image-manager/image-manager.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ImageManagerComponent);
    return ImageManagerComponent;
}());



/***/ }),

/***/ "./src/app/step-description/step-description.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"step\" *ngFor=\"let step of docs.meta.steps\">\n  <input type=\"text\" class=\"input\" [(ngModel)]=\"step.title\" label=\"title\" placeholder=\"Step Title\">\n  <div class=\"image\">\n      <app-image-manager [(image)]=\"step.image\"></app-image-manager>\n  </div>\n  <div class=\"data\">\n      <label>\n      <strong>Completion Time</strong>\n          {{ step.time | extTime}}\n        <input name=\"time\" type=\"range\" min=\"5\" max=\"120\" step=\"5\" [(ngModel)]=\"step.time\">\n      </label>\n      <h4> Describe the step here!</h4>\n      <md-editor \n        name=\"Content\" \n        [preRender]=\"preRenderFunc\" \n        [(ngModel)]=\"step.description\" \n        [height]=\"'200px'\" \n        [mode]=\"mode\" \n        [options]=\"options\" \n        required maxlength=\"500\">\n      </md-editor>  \n      <label>\n        <input type=\"checkbox\" name=\"help\" [(ngModel)]=\"step.helpWanted\"/> I need Help\n      </label>\n      <div *ngIf=\"step.helpWanted\" class=\"help\">\n        <h4>Decribe your problem</h4>\n        <md-editor \n          name=\"Content\" \n          [preRender]=\"preRenderFunc\" \n          [(ngModel)]=\"step.helpDescription\" \n          [height]=\"'200px'\" \n          [mode]=\"mode\" \n          [options]=\"options\" \n          required maxlength=\"500\">\n        </md-editor>  \n\n      </div>\n  </div>    \n</div>\n\n\n<button class=\"button\" (click)=\"docs.addStep()\"> Add Step </button>\n\n"

/***/ }),

/***/ "./src/app/step-description/step-description.component.scss":
/***/ (function(module, exports) {

module.exports = ".step {\n  display: -ms-grid;\n  display: grid;\n  -ms-grid-columns: 1fr;\n      grid-template-columns: 1fr;\n  grid-gap: 10px; }\n  .step div {\n    padding: 20px; }\n  .step .help {\n    padding: 0; }\n"

/***/ }),

/***/ "./src/app/step-description/step-description.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StepDescriptionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__docs_service__ = __webpack_require__("./src/app/docs.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StepDescriptionComponent = /** @class */ (function () {
    function StepDescriptionComponent(docs) {
        this.docs = docs;
    }
    StepDescriptionComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], StepDescriptionComponent.prototype, "title", void 0);
    StepDescriptionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-step-description',
            template: __webpack_require__("./src/app/step-description/step-description.component.html"),
            styles: [__webpack_require__("./src/app/step-description/step-description.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__docs_service__["a" /* DocsService */]])
    ], StepDescriptionComponent);
    return StepDescriptionComponent;
}());



/***/ }),

/***/ "./src/app/utils.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = slugify;
function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
}


/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map